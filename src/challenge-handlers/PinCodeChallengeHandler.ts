/// <reference path="../../plugins/cordova-plugin-mfp/typings/worklight.d.ts" />


const init = () => {
  const PinCodeChallengeHandler = WL.Client.createSecurityCheckChallengeHandler("PinCodeAttempts");

  PinCodeChallengeHandler.handleChallenge = (challenge: any) => {
    let msg = "";

    // Create the title string for the prompt
    if (challenge.errorMsg !== null) {
      msg = challenge.errorMsg + "\n";
    }
    else {
      msg = "This data requires a PIN code.\n";
    }
    msg += "Remaining attempts: " + challenge.remainingAttempts;

    // Display a prompt for user to enter the pin code
    const pinCode = prompt(msg, "");

    if (pinCode) { // calling submitChallengeAnswer with the entered value
      PinCodeChallengeHandler.submitChallengeAnswer({ "pin": pinCode });
    }
    else { // calling cancel in case user pressed the cancel button
      PinCodeChallengeHandler.cancel();
    }
  };

  // handleFailure
  PinCodeChallengeHandler.handleFailure = (error) => {
    WL.Logger.debug("Challenge Handler Failure!");
    if (error.failure !== null && error.failure !== undefined) {
      alert(error.failure);
    }
    else {
      alert("Unknown error");
    }
  };
}

export default {
  init
}
