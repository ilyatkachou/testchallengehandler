import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import ch from '../../challenge-handlers/PinCodeChallengeHandler';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  }

  getPinCode() {
    console.log('getPinCode works!');
    ch.init();
    const resourceRequest = new WLResourceRequest("/adapters/ResourceAdapter/balance", WLResourceRequest.GET);
    resourceRequest.send().then(
      response => {
        WL.Logger.debug("resourceRequest.send success: " + response.responseText);
        document.getElementById("balanceLabel").innerHTML = response.responseText;
      },
      response => {
        WL.Logger.debug("resourceRequest.send success: " + response.errorMsg);
        document.getElementById("balanceLabel").innerHTML = response.errorMsg;
      });
  }

}
